#ifndef NEWMEMORY_H_
#define NEWMEMORY_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define memstart   0x20000000 	// memstart as defined in the assignment
#define CHUNKSIZE  0x4000 		// defining the chunksize for each memory block // 0x4000 = 16384bytes
#define BYTE_ONE   0x80  		//Defining byte offsets for later math and initialization
#define BYTE_TWO   0x100 		//
#define BYTE_THREE 0x200 		//
#define BYTE_FOUR  0x400 		//
#define BYTE_FIVE  0x800 		//

void initMem();					//initialization function
uint32_t * alloc(int mem_size); // allocation function definition
bool dealloc(uint32_t *ptr);	// deallocation function definition
// Structures for holding memory addresses
struct first_mem
{
	uint32_t address;
	bool is_avail;
};
struct second_mem
{
	uint32_t address;
	bool is_avail;
};
struct third_mem
{
	uint32_t address;
	bool is_avail;
};
struct fourth_mem
{
	uint32_t address;
	bool is_avail;
};
struct fifth_mem
{
	uint32_t address;
	bool is_avail;
};
//Arrays of the structures. externed so they can be used in the other files.
extern struct first_mem  block_one_array[128];
extern struct second_mem block_two_array[64];
extern struct third_mem  block_three_array[32];
extern struct fourth_mem block_four_array[16];
extern struct fifth_mem  block_five_array[8];

#endif /* NEWMEMORY_H_ */
