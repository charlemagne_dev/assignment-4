
#ifndef TIMECLOCK_H_
#define TIMECLOCK_H_

struct clock {
	int milisecond;
	int second;
	int minute;
	int hour;
};
struct clock nowtime;
struct clock stopwatch;

void clockplusplus();
void showtime();
void stop_collaborate_and_watch();
void stopwatch_go();

#endif /* TIMECLOCK_H_ */
