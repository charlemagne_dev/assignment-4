/*
 *
 * UART test code:
 * Should echo input from the keyboard on the UART console
 * Caveat emptor!
 * 30 Jan 2013
 *
 * This code works and echoes characters.  Neither the FIFOs nor the Receive-Timer interrupt
 * are used.  This makes the UART function as a run-of-the-mill one-byte-one-interrupt UART.
 * 4 Feb 2013
 */
#include <stdio.h>
#include "uart.h"
#include "queue.h"
#include "cursor.h"
#include "control.h"
#include "timeclock.h"
#include "systick.h"
extern int flag;
extern volatile int stopflag;
extern volatile int stoptick;

void UART0_Init(void)
{
/* Initialize UART0 */
SYSCTL_RCGC1_R |= SYSCTL_RCGC1_UART0; 	// Enable Clock Gating for UART0
SYSCTL_RCGC2_R |= SYSCTL_RCGC2_GPIOA; 	// Enable Clock Gating for PORTA

UART0_CTL_R &= ~UART_CTL_UARTEN;      	// Disable the UART

// Setup the BAUD rate
UART0_IBRD_R = 8;	// IBRD = int(16,000,000 / (16 * 115,200)) = int(8.68055)
UART0_FBRD_R = 44;	// FBRD = int(0.68055 * 64 + 0.5) = 44.0552

UART0_LCRH_R = (UART_LCRH_WLEN_8);	// WLEN: 8, no parity, one stop bit, without FIFOs)
#ifdef RECV_TIMER
UART0_IFLS_R &= ~(UART_RX_FIFO_ONE_EIGHT); 		// Set to interrupt at >= 1/8th of RX FIFO
UART0_IFLS_R &= ~(UART_TX_FIFO_SVN_EIGHT); 		// Set to interrupt at <= 7/8th of TX FIFO
#endif
UART0_CTL_R = UART_CTL_UARTEN;        // Enable the UART and End of Transmission Interrupts

GPIO_PORTA_AFSEL_R |= EN_RX_PA0 | EN_TX_PA1;    	// Enable Receive and Transmit on PA1-0
GPIO_PORTA_DEN_R |= EN_DIG_PA0 | EN_DIG_PA1;   		// Enable Digital I/O on PA1-0

}

void IntEnable(unsigned long InterruptIndex)
{
/* Indicate to CPU which device is to interrupt */
if(InterruptIndex < 32)
	NVIC_EN0_R = 1 << InterruptIndex;		// Enable the interrupt in the EN0 Register
else
	NVIC_EN1_R = 1 << (InterruptIndex - 32);	// Enable the interrupt in the EN1 Register
}

void UART0_IntEnable(unsigned long flags)
{
/* Set specified bits for interrupt */
UART0_IM_R |= flags;
}

void UART0_IntDisable(unsigned long flag)
{
	UART0_IM_R = UART0_IM_R & ~flag;
}

void UART0_Receive(void)
{
	UART0_IntDisable(UART_INT_RX);
	SysTickIntDisable();
	Data = UART0_DR_R;
	enqueue(q1, Data);

	if(stopflag == 1)
		{
		//enqueue(q3,0x01);
		stopflag = 0;
		}
	Control();
	//cursor();
	UART0_DR_R = Data;
	SysTickIntEnable();
	UART0_IntEnable(UART_INT_RX);

}

void UART0_Transmit(void)
{
	int i = 0;
	UART0_IntDisable(UART_INT_TX);
	for(i=0;i<11000;i++); // Delay for proper output
	UART0_DR_R = dequeue(q2);
	UART0_IntEnable(UART_INT_TX);
}

void UART0_IntHandler(void)
{

	/*
 * Simplified UART ISR - handles receive and xmit interrupts
 * Application signalled when data received
 */
if (UART0_MIS_R & UART_INT_RX)
{
	/* RECV done - clear interrupt and make char available to application */
	UART0_ICR_R |= UART_INT_RX;
	UART0_Receive();
}
#ifdef RECV_TIMER
if (UART0_MIS_R & UART_INT_RT)
{
	/* Receive T/O occurred - clear interrupt */
	UART0_ICR_R |= UART_INT_RT;
}
#endif
if (UART0_MIS_R & UART_INT_TX)
	/* XMIT done - clear interrupt */
	UART0_ICR_R |= UART_INT_TX;
}

void SetupPIOSC(void)

{
/* Set BYPASS, clear USRSYSDIV and SYSDIV */
SYSCTRL_RCC_R = (SYSCTRL_RCC_R & CLEAR_USRSYSDIV) | SET_BYPASS ;	// Sets clock to PIOSC (= 16 MHz)
}

void IntMasterEnable(void)
{
/* enable CPU interrupts */
__asm("	cpsie	i");
}

void IntMasterDisable(void)
{
	__asm(" cpsid i");
}


extern void UART0_MoveCursor(int lines)
{
	int i;
	for(i=0; i<lines; i++)
	{
		enqueue(q2, ESC);
		enqueue(q2, 0x44);
		enqueue(q2, NUL);
		writeCursor(3);
	}

}

extern void UART0_ReturnCursor()
{
	//enqueue(q2, ESC);
	//enqueue(q2,SQR_BRKT);
	//enqueue(q2,H);
	enqueue(q2,CR);
	enqueue(q2,NUL);
	writeCursor(2);
}

