/*
 * cursor.c
 *
 *  Created on: Oct 20, 2015
 *      Author: User
 */
#include <stdio.h>
#include "cursor.h"
#include "uart.h"
#include "queue.h"
#include "control.h"

/* Calls the function UART0_Transmit to write the transmit
 * queue to the output terminal
 */
void writeCursor(int length)
{
	int i = 0;

	while(i<length)
	{
		//for(j = 0;j<1000;j++);
		UART0_Transmit();
		i++;
	}
}

int newlineCount = 0; // Count of new line characters

/*  Keeps track of the amount of new line characters transmitted
 * and scrolls the screen down once the bottom has been reached. It
 * also
 */
void scrolling(int k)
{
	//printf("SCROLL\n");
	int i;
	if(Data == BKSPCE)
		{
			UART0_IntDisable(UART_INT_RX);
			enqueue(q2,DEL);
			enqueue(q2,NUL);
			//enqueue(q2,FF);
			writeCursor(2);
			k--;
			UART0_IntEnable(UART_INT_RX);
		}
	if(newlineCount == SCROLL_NUM)
		{
			//printf("Time to scroll\n");
			UART0_IntDisable(UART_INT_RX);
			for(i=0;i<25;i++)
			{
				enqueue(q2,ESC);
				enqueue(q2,0x44);
				enqueue(q2,NUL);
				writeCursor(3);
			}
			enqueue(q2,ESC);
			enqueue(q2,SQR_BRKT);
			enqueue(q2,H);
			enqueue(q2,NUL);
			writeCursor(4);
			UART0_IntEnable(UART_INT_RX);
			newlineCount = 0;
		}
}
