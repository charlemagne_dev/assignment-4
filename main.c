/*
 * main.c
 */
#include <stdio.h>
#include <string.h>
#include "queue.h"
#include "memory.h"
#include "uart.h"
#include "control.h"
#include "systick.h"
#include "control.h"
#include "kernel.h"
#include "ProcessFile.h"
#include "Process.h"
#include "svc.h"
void main(void)
{
	/* Initialize Queue */
	initMem();
	q1 = queueInit();
	q2 = queueInit();
	/* Initialize UART */
	SetupPIOSC();								// Set Clock Frequency to 16MHz (PIOSC)
	UART0_Init();								// Initialize UART0
	Kernel_Initialization();
	IntEnable(INT_UART0);						// Enable UART0 interrupts
	UART0_IntEnable(UART_INT_RX | UART_INT_TX);	// Enable Receive and Transmit interrupts
	IntMasterEnable();							// Enable Master (CPU) Interrupts

	/* Register any processes you want */
	reg_proc(proc0,1);
	reg_proc(proc1,1);
	reg_proc(proc2,4);
	reg_proc(proc3,4);
	reg_proc(proc4,3);
	reg_proc(IdleProc,0);
	/* Set running process and begin */
	running = getNextProcess();

	SVC();
	while(1)
	{
		// Run forever
	}
}

