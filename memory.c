// Memory.c

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "memory.h"

/* This file is for the Allocate and Deallocate functions*/

/* Counter initialization for local for loops*/
    int blocks_used_128 = 0;
    int blocks_used_256 = 0;
    int blocks_used_512 = 0;
    int blocks_used_1024 = 0;
    int blocks_used_2048 = 0;
    int one_count=128;
    int two_count=64;
    int three_count=32;
    int four_count=16;
    int five_count=8;
    int i = 0;

    struct first_mem  block_one_array[128];
    struct second_mem block_two_array[64];
    struct third_mem  block_three_array[32];
    struct fourth_mem block_four_array[16];
    struct fifth_mem  block_five_array[8];

uint32_t * alloc(int size)
{
	/*counting the number of free blocks that are left*/
	/*allocate by setting pointer equal to mem at block count * block size*/
	uint32_t *pointer = NULL;
	if((size < 128)&&(blocks_used_128 != 128))
	{
		for(i=0;i<one_count;i++)
		{
			if(block_one_array[i].is_avail == true)
			{
				pointer = (uint32_t *)block_one_array[i].address;
				blocks_used_128++;
				block_one_array[i].is_avail = false;
				break;
			}
		}
	}
	else if((size < 256)&&(blocks_used_256 != 64))
	{
		for(i=0;i<two_count;i++)
		{
			if(block_two_array[i].is_avail == true)
			{
				pointer = (uint32_t *)block_two_array[i].address;
				blocks_used_256++;
				block_two_array[i].is_avail = false;
				//printf("address is: %x\n", block_two_array[i]);
				break;
			}
		}
	}
	else if((size < 512)&&(blocks_used_512 != 32))
	{
		for(i=0;i<three_count;i++)
		{
			if(block_three_array[i].is_avail == true)
			{
				pointer = (uint32_t *)block_three_array[i].address;
				block_three_array[i].is_avail = false;
				blocks_used_512++;
				//printf("address is: %x\n", block_three_array[i]);
				break;
			}
		}
	}
	else if((size < 1024)&&(blocks_used_1024 != 16))
	{
		for(i=0;i<four_count;i++)
		{
			if(block_four_array[i].is_avail == true)
			{
				pointer = (uint32_t *)block_four_array[i].address;
				blocks_used_1024++;
				block_four_array[i].is_avail = false;
				break;
			}
		}
	}
	else if((size < 2048)&&(blocks_used_2048 != 8))
	{
		for(i=0;i<five_count;i++)
		{
			if(block_five_array[i].is_avail == true)
			{
				pointer = (uint32_t *)block_five_array[i].address;
				blocks_used_2048++;
				block_five_array[i].is_avail = false;
				break;
			}
		}
	}
	else
	{
		pointer = NULL; // if no size is available or the size is out of any of the ranges in the code it returns NULL
	}
	return pointer; // returns pointer selected by algorithm
}

bool dealloc(uint32_t *ptr)
{
	/* Function Looks to see what memory block the passed address is in and then  */
	/* looks to see if it can find the specified address and set the data to NULL*/
	/* and also sets its availability flag to being available again (true).      */
	uint32_t pointer = (uint32_t)ptr; // because of a mishap in the code this is here to simplify changing everything
	//printf("Pointer in Deallocate Function: %d\n", pointer);
	//printf("Block Address in dealloc: %x\n", block_one_array[i]);
	if(pointer < memstart)
	{
		//printf("Deallocation failed, pointer invalid\n");
		return 0;
	}
	else if((pointer >= memstart)&&(pointer < (memstart+CHUNKSIZE)))
	{
		//printf("Passed\n");
		for(i=0;i<one_count;i++)
		{
			//printf("Block One Address: %x\n",block_one_array[i].address);
			if((block_one_array[i].address == pointer)&&(block_one_array[i].is_avail == 0))
			{
				//printf("Pointer is in block 1\n");
				block_one_array[i].is_avail = true;
				block_one_array[i].address = NULL;
				blocks_used_128--;
				pointer = NULL;
				//printf("%d\n", blocks_used_128);
				//printf("%x\n", block_one_array[i]);
				//printf("Deallocation success\n");
				return 1;
			}
		}
	}
	else if((pointer >= (memstart+CHUNKSIZE))&&(pointer < (memstart+(CHUNKSIZE*2))))
	{
		printf("Pointer is in block 2\n");
		for(i=0;i<two_count;i++)
		{
			if((block_two_array[i].address == pointer)&&(block_two_array[i].is_avail == false))
			{
				block_two_array[i].is_avail = true;
				block_two_array[i].address = NULL;
				blocks_used_256--;
				pointer = NULL;
				return 1;
			}
		}
	}
	else if((pointer >= (memstart+(CHUNKSIZE*2)))&&(pointer < (memstart+(CHUNKSIZE*3))))
	{
		printf("Pointer is in block 3\n");
		for(i=0;i<three_count;i++)
		{
			if((block_three_array[i].address == pointer)&&(block_three_array[i].is_avail == false))
			{
				block_three_array[i].is_avail = true;
				block_three_array[i].address = NULL;
				blocks_used_512--;
				pointer = NULL;
				return 1;
			}
		}
	}
	else if((pointer >= (memstart+(CHUNKSIZE*3)))&&(pointer < (memstart+(CHUNKSIZE*4))))
	{
		printf("Pointer is in block 4\n");
		for(i=0;i<four_count;i++)
		{
			if((block_four_array[i].address == pointer)&&(block_four_array[i].is_avail == false))
			{
				block_four_array[i].is_avail = true;
				block_four_array[i].address = NULL;
				blocks_used_1024--;
				pointer = NULL;
				return 1;
			}
		}
	}
	else if((pointer >= (memstart+(CHUNKSIZE*4)))&&(pointer < (memstart+(CHUNKSIZE*5))))
	{
		printf("Pointer is in block 5\n");
		for(i=0;i<five_count;i++)
		{
			if((block_five_array[i].address == pointer)&&(block_five_array[i].is_avail == false))
			{
				block_five_array[i].is_avail = true;
				block_five_array[i].address = NULL;
				blocks_used_2048--;
				pointer = NULL;
				return 1;
			}
		}
	}else
	{
		return 0; // if the pointer that is sent cannot be found the function returns a zero and tells the user that the
				  // deallocation was unsuccessful
	}
	return 0;
}

void initMem()
{

	/*Counter Initialization*/
    int i=0;
    int one_count=128;
    int two_count=64;
    int three_count=32;
    int four_count=16;
    int five_count=8;


    /*Memory Initialization*/
    /*
      ---------  --> System memory Start
     |128's    | -->   ||	 ||		||
     |---------|  |
     |256's    | --> CHUNKSIZE
     |---------|  |
     |512's    | --> CHUNKSIZE*2
     |---------|  |
     |1024's   | --> CHUNKSIZE*3
     |---------|  |
     |2048's   | --> CHUNKSIZE*4
      ---------
     */

    for(i=0;i<one_count;i++)
    {
    	block_one_array[i].address = memstart + i*BYTE_ONE; // system memory + 128 byte offset multiplied by i to make sure it skips enough 128 byte blocks
    	block_one_array[i].is_avail = true;	//Availability flag
    }
    for(i=0;i<two_count;i++)
    {
    	block_two_array[i].address = memstart + CHUNKSIZE + i*BYTE_TWO; //sysmen + chunksize offset and 256 byte offset
    	block_two_array[i].is_avail = true;
    }
    for(i=0;i<three_count;i++)
    {
    	block_three_array[i].address = memstart + CHUNKSIZE*2 + i*BYTE_THREE; //same algorithm as above
    	block_three_array[i].is_avail = true;
    	//printf("address is: %x\n", block_three_array[i]);
    }
    for(i=0;i<four_count;i++)
    {
    	block_four_array[i].address = (memstart + (CHUNKSIZE*3)+(i*BYTE_FOUR)); //same algorithm as above
    	block_four_array[i].is_avail = true;
    }
    for(i=0;i<five_count;i++)
    {
    	block_five_array[i].address = (memstart + (CHUNKSIZE*4)+(i*BYTE_FIVE)); //same algorithm as above
    	block_five_array[i].is_avail = true;
    }
}
