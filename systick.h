/*
 * systick.h
 *
 *  Created on: 2015-10-20
 *      Author: Mike
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#define NVIC_ST_CTRL_R   (*((volatile unsigned long *)0xE000E010))  // SysTick Control and Status Register (STCTRL)
#define NVIC_ST_RELOAD_R (*((volatile unsigned long *)0xE000E014))  // Systick Reload Value Register (STRELOAD)

// SysTick defines

#define NVIC_ST_CTRL_COUNT      0x00010000  // Count Flag for STCTRL
#define NVIC_ST_CTRL_CLK_SRC    0x00000004  // Clock Source for STCTRL
#define NVIC_ST_CTRL_INTEN      0x00000002  // Interrupt Enable for STCTRL
#define NVIC_ST_CTRL_ENABLE     0x00000001  // Enable for STCTRL

#define ST_CTRL 	(*((volatile unsigned long*)(0xE000E010)))	// Control Register Address
#define ST_Reload 	(*((volatile unsigned long*)(0xE000E014)))	// Reload Register Address
#define ST_CurrVal	(*((volatile unsigned long*)(0xE000E018)))	// Current Value Register Address
#define ST_EN 		0x00000001//							// Enable bit
#define INTEN		0x00000002//							// Interupt Enable Bit
#define CLK_SRC 	0x00000004//							// Clock Source bit
//#define RELOAD_VAL  0x186A00									// Reload Value 0->99 = 100
#define RELOAD_VAL 0x400000
#define COUNT		0x0001										// Count Bit
unsigned long tempreg;
unsigned long reloadtemp;

void systick_init();
void SysTickStart(void);
void SysTickStop(void);
void SysTickPeriod(unsigned long Period);
void SysTickIntEnable(void);
void SysTickIntDisable(void);


#endif /* SYSTICK_H_ */
