
#ifndef SVC_H_
#define SVC_H_
#define NVIC_INT_CTRL_R (*((uint32_t volatile *)0xE000ED04))
#define TriggerPendBit  0x10000000

#include "process.h"
void SVCEntry(void);
void SVCHandler(StackFrame *argptr);

#endif /* SVC_H_ */
