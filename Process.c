/*
 * Process.c
 * Support functions
CHANGES

*/
#include <stdio.h>
#include "Process.h"
#include "ProcessFile.h"
#include "memory.h"
#include "kernel.h"

unsigned long get_PSP(void){
    // Move special register PSP to R0
    // since compiler uses R0 to return the value,
	__asm("	mrs r0, psp");

    // Since LR already has the return address, we can simply return by:
	__asm("	bx lr");

    // the following line is not executed but we need to put it add it, since
    // this function claims to return a value, so we need to add to get
    // around compiler error.

	return 0;
}


unsigned long get_MSP(void){
    // Move special register MSP to R0
	__asm("	mrs r0, msp");
	__asm("	bx lr");
	return(0);
}


void set_PSP(volatile unsigned long ProcessStack){
    // Move register R0 to special register PSP
	__asm("	msr	psp, r0");
}


void set_MSP(volatile unsigned long MainStack){
	__asm("	msr	msp, r0");
}

void save_registers(){
    // Move special register PSP to R0
	__asm("	mrs r0,psp");

    // Store multiple, decrement before (i.e. decrement R0 before each access);
    // this will push R4-R11 on to stack,
	__asm("	stmdb r0!,{r4-r11}");

    // Update PSP with the value of R0
	__asm("	msr	psp,r0");
}


void restore_registers(){
    // Move special register PSP to R0
	__asm("	mrs	r0,psp");

    // Load multiple, increment after
    // this will pop R4-R11 from to stack pointed to by R0 (or really PSP)
	__asm("	ldmia r0!,{r4-r11}");

    // Update PSP with the value of R0
	__asm("	msr	psp,r0");
}

unsigned long get_SP(){
	__asm(" 	mov r0,SP");
	__asm("		bx lr");
	return 0;
}

/*
 * Assign 'data' to R7; since the first argument is R0, this is
 * simply a MOV for R0 to R7
 */
void setR7(volatile unsigned long data){
	 __asm("	mov r7,r0");
}



int reg_proc(void (*func_name)(), unsigned priority)
{
	if(priority > 4){ return -1;}
	struct PCB *myPCB;
	//myPCB = (struct PCB*)alloc(sizeof(struct PCB)); //dynamic memory for PCB

	myPCB = (struct PCB*)alloc(sizeof(struct PCB));
	/*PCB init*/
	myPCB->sp = (unsigned long)alloc(STACKSIZE);
	if(myPCB->sp == NULL)
	{
		return -1;
	}
	myPCB->sp = myPCB->sp + STACKSIZE - sizeof(StackFrame);
	StackFrame * mystack = ((StackFrame*)myPCB->sp);
	myPCB->pid = nextID;
	nextID++;
	myPCB->priority = priority;

	//void *process = &TerminateProcess;
	/*StackFrame Registers*/
	mystack->xpsr = 0x01000000;
	mystack->pc = (unsigned long)func_name;
	mystack->lr = (unsigned long)Call_ProcTerminate;
	/* R-Registers */
	mystack->r0  = 0x10101010;
	mystack->r1  = 0x11111111;
	mystack->r2  = 0x12121212;
	mystack->r3  = 0x13131313;
	mystack->r4  = 0x00000000;
	mystack->r5  = 0x00000000;
	mystack->r6  = 0x00000000;
	mystack->r7  = 0x00000000;
	mystack->r8  = 0x00000000;
	mystack->r9  = 0x19191919;
	mystack->r10 = 0x00000000;
	mystack->r11 = 0x00000000;
	mystack->r12 = 0x00000000;
	linkProcess(myPCB);
	printf("registered process: priority %d; id %d; address %lx; stack pointer %lx; sp->lr %lx\n",myPCB->priority, myPCB->pid, mystack->pc, mystack, mystack->lr);
	return myPCB->pid;
}
