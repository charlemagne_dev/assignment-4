/*
 * Kernel.c
 * This source file contains all of the functions to initialize
 * and act on the kernel level
 */
#include "Process.h"
#include "kernel.h"
#include "uart.h"
#include "queue.h"

struct PCB *running;
struct PCB **pQueue;
int nextID;
int num_priorities = 5;

/*
 * Makes a call to the SVC to call a function from the kernel by
 * sending the function number being requested and the argument
 * for the function, if necessary
 */
int Call_SVC(int code, int arg)
{
	volatile struct kcallargs getidarg; /* Volatile to actually reserve space on stack */
	getidarg.code = code; // Sets code number
	getidarg.arg1 = arg; // Sets code argument
	/* Assign address of getidarg to R7 */
	setR7((unsigned long) &getidarg); // Puts pointer to arguments in R7

	SVC();

	return getidarg.rtnvalue;
}
/*
 * Calls the process terminate function through an SVC call
 */
int Call_ProcTerminate()
{
	volatile struct kcallargs getidarg; /* Volatile to actually reserve space on stack */
	getidarg.code = TERMINATE;
	/* Assign address of getidarg to R7 */
	setR7((unsigned long) &getidarg);

	SVC();

	return getidarg.rtnvalue;
}

/*
 * Gets the next highest priority process and returns it
 */
struct PCB* getNextProcess()
{

	int i=0;
	struct PCB * retVal;
	for(i = num_priorities-1; i>=0;i--)
	{
		if(pQueue[i] != NULL)
		{
			retVal = pQueue[i];
			pQueue[i] = pQueue[i]->next;
			return retVal;
		}
	}

	return retVal;
}
/*
 * Links a process to a linked list and chooses which linked list based on the
 * priority of the process
 */
void linkProcess(struct PCB* process)
{
	int p = process->priority;
	if(pQueue[p] == NULL)
		{
		pQueue[p] = process;
		pQueue[p]->next = pQueue[p];
		pQueue[p]->prev = pQueue[p];
		}else{
		process->next = pQueue[p];
		process->prev = pQueue[p]->prev;
		pQueue[p]->prev->next = process;
		pQueue[p]->prev = process;
		}
}
/*
 * Terminates the process that is sent to it as an argument and deallocates it memory
 */
void TerminateProcess(struct PCB * process) //removes process from linked list and deallocates
{
	//printf("Terminating process: pid %d\n",process->pid);
	//remove from linked list
	//dealloc stackframe and PCB
	//getnextprocess();
	//set_psp kinda deal and find the next running

	UnlinkProcess(process);
	dealloc((uint32_t*)process);
}
/*
 * Removes the selected process from the linked list that it is currently in
 */
void UnlinkProcess(struct PCB* process) //remove process from linked list
{
	if(process->next == process) // only one process in list
		{
			pQueue[process->priority] = NULL;

		}
		else
		{
			// Links the processes that were adjacent to the one being unlinked
			process->prev->next = process->next;
			process->next->prev = process->prev;
		}
		//need to call getNextProcess after this so running is not NULL
		/*End Charlo code*/
}

/*
 * Switches the current running process to the one with the highest priority
 */
void ContextSwitch()
{
	UART0_ReturnCursor();
	running->sp = get_PSP();
	running = getNextProcess();
	set_PSP(running->sp);
	UART0_IntDisable(UART_INT_RX);
	UART0_MoveCursor(1);
	UART0_IntEnable(UART_INT_RX);
}
/*
 * Initializes the kernel and allocates the required memory
 */
void Kernel_Initialization()
{

	num_priorities = 5;
	nextID = 0;
	pQueue = ( struct PCB**)alloc(num_priorities *sizeof(struct PCB*));
	int i=0;
	for(i = 0;i<num_priorities;i++)
	{
		pQueue[i] = NULL;
	}

}
/*
 * Promotes the processes priority level based on the argument sent
 */
void promotedBy(int promotions)
{

	if((running->priority + promotions) > 4)
	{
		printf("not a valid priority");
	}
	else
	{
		printf("Promoted successfully\n");
		running->priority += promotions;
		UnlinkProcess(running);
		linkProcess(running);
	}
	ContextSwitch();
}
/*
 * Demotes the process's priority based on the argument sent
 */
void demotedBy(int demotions) // also take a process?
{
	if((running->priority - (unsigned long)demotions) < 0)
	{
		printf("not a valid priority");
	}
	else
	{
		running->priority -= demotions;
		UnlinkProcess(running);
		linkProcess(running);
	}
	ContextSwitch();

}

