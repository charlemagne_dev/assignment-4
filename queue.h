/*
 * queue.h
 *
 *  Created on: Oct 14, 2015
 *      Author: User
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#define MAX_QUEUE 100 // Probably need to switch this out
#define NULL 0

typedef char queueData;

typedef struct queue {
	queueData contents[MAX_QUEUE];
	int front;
	int count;
} queue;

//typedef struct queue
//	*queue *;
queue * q1;
queue * q2;
queue * q3;
/* Queue Functions */
queue * queueInit(void);
void destroyQueue(queue * queue1);
void enqueue(queue * queue1, queueData data);
queueData dequeue(queue * queue1);
int QueueIsEmpty(queue * queue1);

#endif /* QUEUE_H_ */
