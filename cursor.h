/*
 * cursor.h
 *
 *  Created on: Oct 20, 2015
 *      Author: User
 */

#ifndef CURSOR_H_
#define CURSOR_H_

#define NUL	0x00
#define ESC	0x1b
#define FF  0x0c
#define DEL 0x7f
#define BKSPCE 0x08
#define CR 0x0d
#define NEWLINE 0x0A
#define SCROLL_NUM 24
#define SQR_BRKT 0x5b
#define H 0x48

void cursor(void);
void writeCursor(int length);
void cursorMove(void);
void scrolling(int k);

extern int newlineCount;
//char SetBackgroundGreen[] = {ESC, '[', '4', '2', 'm', NUL, FF};

typedef struct{
	char esc;
	char sqrbrkt;
	char row[2];	/* 01 through 24 */
	char semicolon;
	char col[2];	/* 01 through 80 */
	char cmdchar;
	char nul;
} CursorPosition;

//CursorPosition cursorPosition = {ESC, '[', '0', '0', ';', '0', '0', 'H', NUL};

#endif /* CURSOR_H_ */
