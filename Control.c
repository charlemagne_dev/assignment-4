/* Main.c */

/* Questions:
 * how to deal with the TV-100 commands ie are we using scanf statements to read in ESC codes
 * Implementing allocate and deallocate so how should we improve ours
 * how to test our interrupt handlers??
 */

/* Check list
 * 1. Build a queue that can perform FIFO operations CHECK
 * 2. Build a systick handler that will enqueue a systick call CHECK
 * 3. UART that will receive messages from console
 * 4. Control system that will perform the TIME SET AND GO functions HALF CHECK*/

#include "Control.h"
#include "uart.h"
#include "memory.h"
#include "queue.h"
#include "systick.h"
#include "timeclock.h"
#include "cursor.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>

#define SYSTICK_CHAR 0xff
#define TIMEMAX 8 // 11 chars in time string
#define NUM_CMDS 3 // three commands to process
#define HOURCONVERSION 60*60*10
#define MINUTECONVERSION 60*10
#define SECCONVERSION 10
volatile int clocktick;
volatile int stoptick;
volatile int stopflag;
int flag;
volatile int k=0;
void Control()
{

	int nextinline = 4;
	char ctrl_string[messagestring];
	char cntrl_msg;
	const char * timeStr = "time";
	const char * goStr = "go";
	const char * setStr = "set";

	cntrl_msg = dequeue(q1);

	ctrl_string[k] = cntrl_msg;
	scrolling(k);
	k++;

	if(cntrl_msg == 0x0D)
	{
		ctrl_string[k] = '\0';
		if(memcmp(ctrl_string,timeStr,4) == 0)
		{
			nextinline = TIME;
			//printf("FOUND TIME\n");
		}
		else if(memcmp(ctrl_string,goStr,2) == 0)
		{
			nextinline = GO;
			//printf("FOUND GO\n");
		}
		else if(memcmp(ctrl_string,setStr,3) == 0)
		{
			nextinline = SET;
			//printf("FOUND SET\n");
		}
		k = 0;
		stopflag = 0;
		switch(nextinline)
		{
		case TIME:
			//printf("Showtime\n");
			showtime();
			break;
		case SET:
			nowtime.hour  	   = ((ctrl_string[4] -'0')*10) + ctrl_string[5] - '0';
			nowtime.minute	   = ((ctrl_string[7] -'0')*10) + ctrl_string[8] - '0';
			nowtime.second	   = ((ctrl_string[10]-'0')*10) + ctrl_string[11]- '0';
			nowtime.milisecond = (ctrl_string[13] -'0');
			clocktick = nowtime.hour*60*60*10 + nowtime.minute*60*10 + nowtime.second*10+ nowtime.milisecond;
			showtime();
			break;
		case GO:
			printf("Go time\n");
			/*
			flag = 1;
			while(!QueueIsEmpty(q3))
			{
				enqueue(q3);
			}
			*/
			stopflag = 1;
			stoptick = 0;
			stopwatch.milisecond = 0;
			stopwatch.second = 0;
			stopwatch.minute = 0;
			stopwatch.hour = 0;

			//UART0_IntDisable( UART_INT_RX);
			//while(1)
			//{
				//stopwatch_go();
				//stop_collaborate_and_watch();
				//if(dequeue(q3) != (0x00))
				//{
				//	break;
				//}
				//if(stopflag == 0)
				//{
				//	break;
				//}*/
			//}
			break;
		default:
			enqueue(q2,'\r');
			UART0_Transmit();
			enqueue(q2,'\n');
			UART0_Transmit();
			enqueue(q2,'?');
			UART0_Transmit();
			enqueue(q2,'\0');
			UART0_Transmit();
			enqueue(q2,'\r');
			UART0_Transmit();
			enqueue(q2,'\n');
			UART0_Transmit();
			newlineCount+=2;
			break;
			}// end of switch
	}//END OF ELSE
	UART0_IntDisable(UART_INT_TX);
	UART0_IntEnable(UART_INT_TX);
// end of function
}
