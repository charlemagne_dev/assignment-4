
#ifndef KERNEL_H_
#define KERNEL_H_

#define TRUE 1
#define FALSE 0

enum kernelcallcodes {SWITCH, GETID, NICE, TERMINATE, PROMOTE, DEMOTE};
#define NVIC_SYS_PRI3_R (*((volatile unsigned long *) 0xE000ED20))
#define PENDSV_LOWEST_PRIORITY 0x00E00000

struct kcallargs
{
unsigned int code;
unsigned int rtnvalue;
unsigned int arg1;
unsigned int arg2;
};

/*GLOBALS*/
extern struct PCB *running;
extern struct PCB **pQueue;
extern int num_priorities;
extern int nextID;

int Call_SVC(int code, int arg);
int Call_ProcTerminate(); // calls SVC with TERMINATE as code. Set process LR register to this so it terminates on completion

void promotedBy(int promotions);
void demotedBy(int demotions);
void ContextSwitch();
void UnlinkProcess(struct PCB* process);
void TerminateProcess(struct PCB* process);
void linkProcess(struct PCB* process);
struct PCB* getNextProcess();
void Kernel_Initialization();
#endif /* KERNEL_H_ */
