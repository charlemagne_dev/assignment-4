/*
 * queue.c
 *
 *  Created on: Oct 14, 2015
 *      Author: User
 */
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include "memory.h"


queue * queueInit(void)
{
	queue * queue1;
	queue1 = (queue *)alloc(sizeof(queue)); // replace with our alloc function

	if(queue1 == NULL){
		printf("NOT ENOUGH MEMORY FOR QUEUE\n");
		exit(EXIT_FAILURE);
	}
	queue1->front = 0;
	queue1->count = 0;
	return queue1;
}

void destroyQueue(queue * queue1)
{
	dealloc((uint32_t *)queue1); // Replace with our dealloc function
}

void enqueue(queue * queue1, queueData data)
{
	int newData;
	if(queue1->count >= MAX_QUEUE)
	{
		printf("Queue is full\n");
		//exit(EXIT_FAILURE);
	}
	else
	{
		newData = (queue1->front + queue1->count) % MAX_QUEUE;
		queue1 -> contents[newData] = data;
		queue1 -> count++;
	}
}

queueData dequeue(queue * queue1)
{
	queueData oldData;
	if(queue1->count <=0)
	{
		//printf("Queue is empty\n");
		//exit(EXIT_FAILURE);
	}
	oldData = queue1->contents[queue1->front];

	queue1->front++;
	queue1->front %= MAX_QUEUE;
	queue1->count--;

	return oldData;

}

int QueueIsEmpty(queue * queue1)
{
	if(queue1 -> count == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
