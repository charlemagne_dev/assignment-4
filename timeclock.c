#include <stdio.h>
#include <stdlib.h>
#include "systick.h"
#include "uart.h"
#include "queue.h"
#include "control.h"
#include "timeclock.h"
#include "cursor.h"

#define MAX_HOUR 23
#define CHAR_CONVERSION 48
#define STOP_STRING_LENGTH 12
#define TIME_STRING_LENGTH 14

volatile int clocktick;
volatile int stoptick;
volatile int stopflag;

void clockplusplus()
{
	clocktick++; // increment counter
	int clockmodifier = clocktick/10; //for single numbers
	int clockremainder = clockmodifier % 3600; // remainder after division and mod
	if(stopflag == 1) // is stopwatch set?
	{
		stoptick++;
        stopwatch_go();
		stop_collaborate_and_watch();
	}
	else if(stopflag == 0)
	{
		stoptick = 0;
	}
	if(nowtime.hour > MAX_HOUR) // make sure it doesnt overflow
	{
		nowtime.hour = 0;
		nowtime.minute = 0;
		nowtime.second = 0;
		nowtime.milisecond = 1;
		clocktick = 0;
	}
	else // calculate current time based off of the counter
	{
	nowtime.milisecond = clocktick  % 10;
	nowtime.second = clockremainder % 60;
	nowtime.minute = clockremainder / 60;
	nowtime.hour = clockmodifier / 3600;
	}

}

void showtime() // display the current time
{
	char writetime[TIME_STRING_LENGTH];
	writetime[0] = '\n';
	writetime[1] = '\r';
	writetime[2] = ((nowtime.hour / 10) + CHAR_CONVERSION);
	writetime[3] = ((nowtime.hour % 10) + CHAR_CONVERSION);
	writetime[4] = ':';
	writetime[5] = ((nowtime.minute / 10) + CHAR_CONVERSION);
	writetime[6] = ((nowtime.minute % 10) + CHAR_CONVERSION);
	writetime[7] = ':';
	writetime[8] = ((nowtime.second / 10) + CHAR_CONVERSION);
	writetime[9] = ((nowtime.second % 10) + CHAR_CONVERSION);
	writetime[10] = '.';
	writetime[11] = ((nowtime.milisecond) + CHAR_CONVERSION);
	writetime[12] = '\r';
	writetime[13] = '\n';
	//writetime[12] = '\0';
	int i;
	for(i=0;i<TIME_STRING_LENGTH;i++)
	{
		enqueue(q2,writetime[i]); // enqueue to write
	}
	i = 0;
	while(i<TIME_STRING_LENGTH)
	{
		UART0_Transmit(); // write to uart
		i++;
	}
	newlineCount += 2; // for scrolling, 2 newlines occur so counter + 2
	//printf("Current Time: %d : %d : %d : %d\n", nowtime.hour, nowtime.minute, nowtime.second, nowtime.milisecond);
}

void stopwatch_go() //stopwatch, same as clockplusplus only with seperate variables
{
	int stopclockmodifier = stoptick/10;
	stopwatch.hour = stopclockmodifier / 3600;
	stopwatch.milisecond = stoptick % 10;
	int stopclockremainder = stopclockmodifier % 3600;
	stopwatch.minute = stopclockremainder / 60;
	stopwatch.second = stopclockremainder % 60;


}

void stop_collaborate_and_watch() // display stopwatch
{
	char writetime[STOP_STRING_LENGTH];
	writetime[0] = '\r';
	writetime[1] = ((stopwatch.hour / 10) + CHAR_CONVERSION);
	writetime[2] = ((stopwatch.hour % 10) + CHAR_CONVERSION);
	writetime[3] = ':';
	writetime[4] = ((stopwatch.minute / 10) + CHAR_CONVERSION);
	writetime[5] = ((stopwatch.minute % 10) + CHAR_CONVERSION);
	writetime[6] = ':';
	writetime[7] = ((stopwatch.second / 10) + CHAR_CONVERSION);
	writetime[8] = ((stopwatch.second % 10) + CHAR_CONVERSION);
	writetime[9] = '.';
	writetime[10] = ((stopwatch.milisecond) + CHAR_CONVERSION);
	writetime[11] = '\r';

	int i;
	for(i=0;i<STOP_STRING_LENGTH;i++)
	{
		enqueue(q2,writetime[i]);
	}
	i = 0;
	while(i<STOP_STRING_LENGTH)
	{
		UART0_Transmit();
		i++;
	}
	//printf("Current Time: %d : %d : %d : %d\n", stopwatch.hour, stopwatch.minute, stopwatch.second, stopwatch.milisecond);
}
