
#include <stdio.h>
#include <stdlib.h>
#include "uart.h"
#include "control.h"
#include "systick.h"
#include "memory.h"
#include "queue.h"
#include "timeclock.h"
#include "kernel.h"
#include "svc.h"

volatile int stopflag;
volatile int stoptick;
void systick_init() // set register values
{
	/*Systick Setup*/
	/*Control Register*/
	tempreg  	= ST_CTRL;
	tempreg 	|= ST_EN;
	tempreg 	|= CLK_SRC;
	tempreg 	|= INTEN;
	ST_CTRL  	= tempreg;
	/*Reload Register*/
	reloadtemp  = ST_Reload;
	reloadtemp 	|= RELOAD_VAL;
	ST_Reload   = reloadtemp;

	/*Test Print*/
	//printf("ST_CTRL: %lx \nST_reload: %lx\n", ST_CTRL, ST_Reload);
}

void systick_handler() // handles systick interrupts
{
	//printf("Systick\n");
	SysTickIntDisable();

	save_registers();
	ContextSwitch();
	//Call_SVC(SWITCH,0);
	restore_registers();



	SysTickIntEnable();
}

// SysTick NVIC Registers



void SysTickStart(void)
{
// Set the clock source to internal and enable the counter
NVIC_ST_CTRL_R |= NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_ENABLE;
}

void SysTickStop(void)
{
// Clear the enable bit to stop the counter
NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_ENABLE);
}

void SysTickPeriod(unsigned long Period)
{
// Must be between 0 and 16777216
NVIC_ST_RELOAD_R = Period - 1;
}

void SysTickIntEnable(void) // interrupt enabler
{
// Set the interrupt bit in STCTRL
NVIC_ST_CTRL_R |= NVIC_ST_CTRL_INTEN;
}

void SysTickIntDisable(void) // Interrupt De-abler
{
// Clear the interrupt bit in STCTRL
NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_INTEN);
}
