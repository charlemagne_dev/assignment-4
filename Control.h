/*Control.h*/
#ifndef CONTROL_H_
#define CONTROL_H_
// Register and bit definition

/*integer declarations*/
#define hourmax 23
#define minutemax 59
#define secmax 59
#define milisecmax 9
#define messagestring 20
#define CR 0x0d

extern volatile int k;
/*Switch Case Enumeration*/
enum
{
	GO 	     = 0,
	TIME	 = 1,
	SET		 = 2

};

/*Function Declarations*/
extern void Control();
//void systick_handler();
#endif /* CONTROL_H_ */
