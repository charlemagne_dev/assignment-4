/*
 * ProcessFile.c
 * Contains all of the functions for any processes that we
 * may want to call.  All of the processes do basically the
 * same thing, printing their process number 25 times, but process
 * 1 promotes itself at a point, and process 2 demotes itself.
 */
#include <stdio.h>
#include "Process.h"
#include "uart.h"
#include "queue.h"
#include "cursor.h"
#include "kernel.h"

void proc0()
{
	//printf("I am Process 0\n");
	long long i;
	for(i=0; i<25; i++) {
		printf("P0 running\n");
		UART0_IntDisable(UART_INT_RX);
		enqueue(q2,'0');
		UART0_IntEnable(UART_INT_RX);
		writeCursor(1);
		long long j;
		for(j=0; j<10000; j++) {
		}
	}
}

void proc1()
{
	long long i;
	for(i=0; i<25; i++) {
		printf("P1 running\n");
		UART0_IntDisable(UART_INT_RX);
		enqueue(q2,'1');
		UART0_IntEnable(UART_INT_RX);
		//if(i == 5)
		//{
		//	printf("PROMOTED P1\n");
			//Call_SVC(PROMOTE,1);
		//}
		writeCursor(1);
		long long j;
		for(j=0; j<10000; j++) {
		}
	}
}

void proc2()
{
	//printf("I am Process 2\n");
	long long i;
		for(i=0; i<25; i++) {
			printf("P2 running\n");
			UART0_IntDisable(UART_INT_RX);
			enqueue(q2,'2');
			UART0_IntEnable(UART_INT_RX);
			if(i==4)
			{
				//printf("Demoted\n");
				Call_SVC(DEMOTE,2);
			}
			writeCursor(1);
			long long j;
			for(j=0; j<10000; j++) {
			}
		}
}

void proc3()
{
	//printf("I am Process 3\n");
	long long i;
		for(i=0; i<25; i++) {
			printf("P3 running\n");
			UART0_IntDisable(UART_INT_RX);
			enqueue(q2,'3');
			UART0_IntEnable(UART_INT_RX);
			writeCursor(1);
			long long j;
			for(j=0; j<10000; j++) {
			}
		}
}

void proc4()
{
	//printf("I am Process 4\n");
	long long i;
		for(i=0; i<25; i++) {
			printf("P4 running\n");
			UART0_IntDisable(UART_INT_RX);
			enqueue(q2,'4');
			UART0_IntEnable(UART_INT_RX);
			writeCursor(1);
			long long j;
			for(j=0; j<10000; j++) {
			}
		}
}
/*
 * This process is run once all other processes have been run
 * when CPU is idling
 */
void IdleProc()
{
	printf("Idle\n");
	while(1);
}
